package main

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

// token is the token that the page has to return
var token = ""

// Let's encrypt (https://zerossl.com/ or https://www.sslforfree.com/)
func main() {
	r := gin.Default()

	r.LoadHTMLGlob("templates/*")

	r.GET("/", mainPage)
	r.GET("/.well-known/acme-challenge/:token", handler)
	r.POST("/token", setToken)

	port := os.Getenv("PORT")
	if port == "" {
		port = "80"
	}
	r.Run(":" + port)
}

func mainPage(c *gin.Context) {
	c.HTML(http.StatusOK, "main.tmpl", gin.H{
		"token": token,
	})
}

func handler(c *gin.Context) {
	c.String(http.StatusOK, token)
}

func setToken(c *gin.Context) {
	newToken, _ := c.GetPostForm("token")
	token = newToken

	c.Redirect(http.StatusMovedPermanently, "/")
}
